<?php
namespace App\Customer;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Customer extends  DB{
    private $name;
    private $email;
    private $phone;
    private $address;
    private $created;


    public function setData($postData){

        if(array_key_exists('customerName',$postData)){
            $this->name = $postData['customerName'];
        }
        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }

        if(array_key_exists('phone',$postData)){
            $this->phone = $postData['phone'];
        }

        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }
        if(array_key_exists('creationDate',$postData)){
            $this->created = $postData['creationDate'];
        }

    }
    public function store(){

        $arrData = array($this->name,$this->email,$this->phone,$this->address,$this->created);
        $sql = "INSERT into customers(name,email,phone,address,created) VALUES(?,?,?,?,?)";

        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }
    public function index(){
        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No'";
         $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }

    public function allClients(){

        $sql = "select * from customers where soft_deleted='No'";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allparticulars(){

        $sql = "select * from products where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function statement(){

        //$sql = "select * from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND salesentry.customer_id =".$this->customer_id."  AND  salesentry.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function statementTotal(){

        $sql = "select sl, sum(amount)  as totalamount  from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql="select sl, sum(amount) as totalamount from expenseincome where soft_deleted='No' AND transactionType =1 AND transactionDate BETWEEN '2015-01-01' AND '2017-01-01'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }

    public function view(){

        $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.customer_id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND salesentry.id=".$this->id;
        //$sql = "select * from expenseincome where sl=3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function trashed(){

        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function update(){

        //$arrData = array($this->book_name,$this->author_name);
       // $sql = "UPDATE  expenseincome SET particulars=?,amount=? WHERE sl=".$this->sl;

        $arrData = array($this->customer_id,$this->modified_Date,$this->product_id,$this->product_rate,$this->bandwidth_quantity,$this->from_DurationDate,$this->to_DurationDate,$this->total_Date,$this->amount_Total,$this->receipt_Invoice);
        $sql = "UPDATE salesentry SET customer_id=?,modified=?,product_id=?,rate=?, bandwidth=?,fromDurationDate=?, toDurationDate=?, totaldays=?,amount=?, receiptInvoiceNo=? WHERE id=".$this->id;


        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');
    }
    public function trash(){


        $sql = "UPDATE  salesentry SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }

    public function recover(){

        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");
        Utility::redirect('index.php');
    }
    public function delete(){

        $sql = "Delete from salesentry  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from salesentry  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from salesentry  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        }catch (PDOException $error){

            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes'";
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  book_title SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");
        Utility::redirect('trashed.php?Page=1');
    } public function recoverMultiple($markArray){

    foreach($markArray as $id){
        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$id;
        $result = $this->DBH->exec($sql);
        if(!$result) break;
    }

    if($result)
        Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
    else
        Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");
    Utility::redirect('index.php?Page=1');

    }

    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "Delete from expenseincome  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('index.php?Page=1');
    }

    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from expenseincome  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

    // start of search()
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND (`product_name` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%')";

        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) )

            $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND `product_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND `receiptInvoiceNo` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) || !isset($requestArray['byAuthor']) && isset($requestArray['id']) )
            $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND `id` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['advanced']))
            $sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No' AND (`product_name` LIKE '%".$requestArray['search']."%' OR `receiptInvoiceNo` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%' OR `PayerPayee` LIKE '%".$requestArray['search']."%')";
        /*
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `expenseincome` WHERE `soft_deleted` ='No' AND (`particulars` LIKE '%".$requestArray['search']."%' OR `receiptInvoiceNo` LIKE '%".$requestArray['search']."%' OR `sl` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `expenseincome` WHERE `soft_deleted` ='No' AND `particulars` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `expenseincome` WHERE `soft_deleted` ='No' AND `receiptInvoiceNo` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) || !isset($requestArray['byAuthor']) && isset($requestArray['sl']) )  $sql = "SELECT * FROM `expenseincome` WHERE `soft_deleted` ='No' AND `sl` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['advanced']))
            $sql = "SELECT * FROM `expenseincome` WHERE `soft_deleted` ='No' AND (`particulars` LIKE '%".$requestArray['search']."%' 
            OR `receiptInvoiceNo` LIKE '%".$requestArray['search']."%' 
            OR `sl` LIKE '%".$requestArray['search']."%' 
            OR `PayerPayee` LIKE '%".$requestArray['search']."%')";

        */
        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()

    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        // for each search field (particulars) block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->product_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->product_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (particulars) block end

           //for each search field () block start
        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (name)  block end


        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receiptInvoiceNo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receiptInvoiceNo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (receiptInvoiceNo)  block end

        // for each search field (sl)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (sl)  block end

        // for each search field (PayerPayee)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->PayerPayee);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->PayerPayee);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (PayerPayee)  block end


        return array_unique($_allKeywords);

    }// get all keywords



}