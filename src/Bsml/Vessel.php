<?php
namespace App\Bsml;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Vessel extends  DB{

    private $modifiedDate, $categoryid;
    private $vesselName;
    private $importerName;
    private $bankId;
    private $lcNo;
    private $lcDate;
    private $ldt;
    private $dollarPrice;
    private $dollarRate;
    private $wastage;
    private $wastageValue;
    private $remarks;



    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modifiedDate = $postData['modifiedDate'];
        }
        if(array_key_exists('vesselName',$postData)){
            $this->vesselName = $postData['vesselName'];
        }
        if(array_key_exists('importerName',$postData)){
            $this->importerName = $postData['importerName'];
        }
         if(array_key_exists('bankId',$postData)){
            $this->bankId = $postData['bankId'];
        }
         if(array_key_exists('lcNo',$postData)){
            $this->lcNo = $postData['lcNo'];
        }
        if(array_key_exists('lcNo',$postData)){
            $this->lcNo = $postData['lcNo'];
        }
         if(array_key_exists('lcDate',$postData)){
            $this->lcDate = $postData['lcDate'];
        }
        if(array_key_exists('ldt',$postData)){
            $this->ldt = $postData['ldt'];
        }
        if(array_key_exists('dollarPrice',$postData)){
            $this->dollarPrice = $postData['dollarPrice'];
        }
        if(array_key_exists('dollarRate',$postData)){
            $this->dollarRate = $postData['dollarRate'];
        }
        if(array_key_exists('wastage',$postData)){
            $this->wastage= $postData['wastage'];
        }
        if(array_key_exists('wastageValue',$postData)){
            $this->wastageValue= $postData['wastageValue'];
        }
        if(array_key_exists('remarks',$postData)){
            $this->remarks= $postData['remarks'];
        }

    }
    public function store(){
        $this->categoryid=1;
 //var_dump($_POST); die();
        $arrData = array($this->modifiedDate,$this->vesselName,$this->categoryid,$this->importerName,$this->bankId,$this->lcNo,$this->lcDate,$this->ldt,$this->dollarPrice,$this->dollarRate,$this->wastage,$this->wastageValue,$this->remarks);
        $sql = "INSERT into ship(created,vesselname,categoryid,importername,bankid,lcno,lcdate,ldt,dollarprice,dollarrate,wastage,wastagevalue,remarks) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result){
            $arrData = array($this->modifiedDate,$this->vesselName,$this->categoryid,$this->importerName,$this->bankId,$this->lcNo,$this->lcDate,$this->ldt,$this->dollarPrice,$this->dollarRate,$this->wastage,$this->wastageValue,$this->remarks);
            $sql = "INSERT into products(created,product_name,categoryid,importername,bankid,lcno,lcdate,ldt,dollarprice,dollarrate,wastage,wastagevalue,remarks) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($sql);
            $queryResult =$STH->execute($arrData);
            if($queryResult)
                Message::message("Success! Data Has Been Inserted Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }

    public function index(){

        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT X.id, products.product_name, customers.name, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id  GROUP BY X .id ";
         $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function allClients(){

        $sql = "select * from customers where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allparticulars(){

        $sql = "select * from products where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    /*public function statement(){

        //$sql = "select * from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        $sql = "SELECT products.product_name, customers.name, salestransaction.id,salestransaction.rate,salestransaction.bandwidth,salestransaction.fromDurationDate,salestransaction.toDurationDate,salestransaction.totaldays,salestransaction.amountOut,salestransaction.amountIn,salestransaction.receiptInvoiceNo,salestransaction.particulars FROM salestransaction INNER JOIN products ON salestransaction.product_id=products.id INNER JOIN customers ON salestransaction.customerId=customers.id where salestransaction.soft_deleted='No' AND salestransaction.customerId =".$this->customer_id."  AND  salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    } */
    public function statementTotal(){

        $sql = "select sl, sum(amount)  as totalamount  from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql="select sl, sum(amount) as totalamount from expenseincome where soft_deleted='No' AND transactionType =1 AND transactionDate BETWEEN '2015-01-01' AND '2017-01-01'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }

    public function view(){
        $sql = "SELECT salestransaction.id,products.id as productid, products.product_name,customers.id as customerid, customers.name, salestransaction.rate,salestransaction.bandwidth,salestransaction.fromDurationDate,salestransaction.toDurationDate,salestransaction.totaldays,salestransaction.amountOut,salestransaction.amountIn,salestransaction.receiptInvoiceNo,salestransaction.particulars FROM salestransaction INNER JOIN products ON salestransaction.product_id=products.id INNER JOIN customers ON salestransaction.customerId=customers.id where salestransaction.soft_deleted='No' AND salestransaction.id =$this->id";
        //$sql = "select * from expenseincome where sl=3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function trashed(){

        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function update(){

        //$arrData = array($this->book_name,$this->author_name);
       // $sql = "UPDATE  expenseincome SET particulars=?,amount=? WHERE sl=".$this->sl;

        $arrData = array($this->customer_id,$this->modified_Date,$this->product_id,$this->product_rate,$this->bandwidth_quantity,$this->from_DurationDate,$this->to_DurationDate,$this->total_Date,$this->amount_Total,$this->receipt_Invoice);
        $sql = "UPDATE salestransaction SET customerId=?,modified=?,product_id=?,rate=?, bandwidth=?,fromDurationDate=?, toDurationDate=?, totaldays=?,amount=?, receiptInvoiceNo=? WHERE id=".$this->id;


        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');
    }
    public function trash(){


        $sql = "UPDATE  salesentry SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }

    public function recover(){

        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");
        Utility::redirect('index.php');
    }
    public function delete(){

        $sql = "Delete from salesentry  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        }catch (PDOException $error){

            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        }catch (PDOException $error){

            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes'";
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  book_title SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");
        Utility::redirect('trashed.php?Page=1');
    } public function recoverMultiple($markArray){

    foreach($markArray as $id){
        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$id;
        $result = $this->DBH->exec($sql);
        if(!$result) break;
    }

    if($result)
        Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
    else
        Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");
    Utility::redirect('index.php?Page=1');

    }

    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "Delete from expenseincome  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('index.php?Page=1');
    }

    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from expenseincome  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

    // start of search()
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND (product_name LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%')GROUP BY X .id ";

        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND product_name LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND x.receiptInvoiceNo LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) || !isset($requestArray['byAuthor']) && isset($requestArray['id']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND X.id='".$requestArray['search']."' GROUP BY X .id";

        if(isset($requestArray['advanced']))
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No'  AND (product_name LIKE '%".$requestArray['search']."%' OR X.receiptInvoiceNo LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%' OR X.voucherNo LIKE '%".$requestArray['search']."%' OR X.remarks LIKE '%".$requestArray['search']."%'  ) GROUP BY X .id ";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//
    // end of search()

    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        // for each search field (particulars) block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->product_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->product_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (particulars) block end

           //for each search field () block start
        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (name)  block end


        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receiptInvoiceNo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receiptInvoiceNo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (receiptInvoiceNo)  block end

        // for each search field (sl)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (sl)  block end

        // for each search field (PayerPayee)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receivedTo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receivedTo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (PayerPayee)  block end

        // for each search field (transactionId)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (transactionId)  block end
        // for each search field (remarks)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->remarks);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->remarks);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (remarks)  block end

        return array_unique($_allKeywords);

    }// get all keywords



}