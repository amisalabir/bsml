<?php
namespace App\Bsml;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Bank extends  DB{

    private $bankname, $accountname, $accountnumber, $branch, $address;

    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];
        }
        if(array_key_exists('bankname',$postData)){
            $this->bankname = $postData['bankname'];
        }
        if(array_key_exists('accountname',$postData)){
            $this->accountname = $postData['accountname'];
        }
         if(array_key_exists('accountnumber',$postData)){
            $this->accountnumber = $postData['accountnumber'];
        }
         if(array_key_exists('branch',$postData)){
            $this->branch = $postData['branch'];
        }
        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }
    }
    public function store(){

        $arrData = array($this->bankname,$this->accountname,$this->accountnumber,$this->branch,$this->address,$this->modified_Date);
        $sql = "INSERT into bank(bankname,accountname,accountnumber,branch,address,created) VALUES(?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Bank Account Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }




}