<?php

namespace App\Bsml;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Bsml extends  DB{
    private $customer_id;
    private $allCustomers;
    private $product_id;
    private $branch_id;
    private $shipId;
    private $ton;
    private $kg;
    private $transaction_Type;
    private $particulars_Details;
    private $account_head;
    private $bandwidth_quantity;
    private $transaction_Date;
    private $from_DurationDate;
    private $to_DurationDate;
    private $amount_Total;
    private $receipt_Invoice;
    private $from_Transaction;
    private $to_Transaction;
    private $id;
    private $sl;
    private $modified_Date;
    private $total_Date;
    private $voucher_no;
    private $crvoucher_no;
    private $challan_no;
    private $transaction_for;
    private $transaction_mode;
    private $received_to;
    private $received_from;
    private $cheque_no;
    private $cheque_date;
    private $remarks;
    private $inWords;
    private $lastSalesId;
    private $result;
    private $shipexheadid;

    public function setData($postData){

        if(array_key_exists('customerId',$postData)){
            $this->customer_id = $postData['customerId'];
        }
        if(array_key_exists('branchid',$postData)){
            $this->branch_id = $postData['branchid']; }

           if(array_key_exists('bankId',$postData)){
            $this->bank_id= $postData['bankId']; }

        if(array_key_exists('all',$postData)){
            $this->allCustomers = $postData['all'];
        }
        if(array_key_exists('transactionType',$postData)){
            $this->transaction_Type = $postData['transactionType'];
        }

        if(array_key_exists('particulars',$postData)){
            $this->particulars_Details = $postData['particulars'];
        }
        if(array_key_exists('accheadId',$postData)){
            $this->account_head = $postData['accheadId'];
        }
        if(array_key_exists('shipid',$postData)){
            $this->product_id = $postData['shipid'];
        }
        if(array_key_exists('tonWeight',$postData)){
            $this->ton = $postData['tonWeight'];
        }
        if(array_key_exists('kgWeight',$postData)){
            $this->kg = $postData['kgWeight'];
        }
        if(array_key_exists('voucherNo',$postData)){
            $this->voucher_no = $postData['voucherNo']; }

        if(array_key_exists('challanNo',$postData)){
            $this->challan_no = $postData['challanNo']; }

        if(array_key_exists('crVoucherNo',$postData)){
            $this->crvoucher_no = $postData['crVoucherNo']; }

        if(array_key_exists('transactionDate',$postData)){
            $this->transaction_Date = $postData['transactionDate'];
        }
        if(array_key_exists('transactionFor',$postData)){
            $this->transaction_for= $postData['transactionFor'];
        }

        if(array_key_exists('transactionMode',$postData)){
            $this->transaction_mode= $postData['transactionMode'];
        }
        if(array_key_exists('receivedTo',$postData)){
            $this->received_to= $postData['receivedTo'];
        }
        if(array_key_exists('receivedFrom',$postData)){
            $this->received_from= $postData['receivedFrom'];
        }
        if(array_key_exists('chequeNo',$postData)){
            $this->cheque_no= $postData['chequeNo'];
        }
        if(array_key_exists('chequeDate',$postData)){
            $this->cheque_date = $postData['chequeDate'];
        }
        if(array_key_exists('remarks',$postData)){
            $this->remarks = $postData['remarks'];
        }

        if(array_key_exists('amount',$postData)){
            $this->amount_Total = $postData['amount'];
        }
        if(array_key_exists('inWords',$postData)){
            $this->inWords = $postData['inWords'];
        }
        if(array_key_exists('fromTransaction',$postData)){
            $this->from_Transaction = $postData['fromTransaction'];
        }
        if(array_key_exists('toTransaction',$postData)){
            $this->to_Transaction = $postData['toTransaction'];
        }
        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }
        if(array_key_exists('transactionid',$postData)){
            $this->id = $postData['transactionid'];
        }
        if(array_key_exists('sl',$postData)){
            $this->sl = $postData['sl'];
        }
        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];
        }
        if(array_key_exists('shipexheadid',$postData)){
            $this->shipexheadid = $postData['shipexheadid'];
        }
        if(array_key_exists('totalDays',$postData)){
            $this->total_Date = $postData['totalDays'];
        }
        if(empty($this->crvoucher_no)||$this->crvoucher_no=='0')$this->crvoucher_no=NULL;
        if(empty($this->voucher_no)|| $this->voucher_no=='0')$this->voucher_no=NULL;
        if(empty($this->challan_no)||$this->challan_no=='0')$this->challan_no=NULL;
        if(empty($this->customer_id)||$this->customer_id=='0')$this->customer_id=NULL;

            }
    public function lastSalesId(){
        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT id from salestransaction  ORDER  BY id DESC LIMIT 1";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function storesales(){
        //var_dump($_POST); die();
        $arrData="";
        $sql="";
        if($_POST['transactionType']=='MREC' && $_POST['accheadId']==24){
            $arrData = array($this->branch_id,$this->lastSalesId,$this->customer_id,$this->transaction_Type,$this->transaction_Date,$this->product_id,$this->particulars_Details,$this->account_head,$this->bank_id,$this->crvoucher_no,$this->challan_no,$this->transaction_for,$this->transaction_mode,$this->received_from,$this->cheque_no,$this->cheque_date,$this->ton,$this->kg,$this->modified_Date,);
            $sql = "INSERT into sales(branchid,salesid,customerId,transactionType,transactionDate,product_id,particulars,accountheadid,bankid,crvoucher,challanno,transactionFor,transactionMode,receivedFrom,chequeNo,chequeDate,tonweight,kgweight,created) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        //echo "<pre>"; var_dump($arrData);echo "</pre>"; die();

        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            //$this->store();
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');


    }
    public function store()
    {

        $arrData = "";
        $sql = "";
        if ($_POST['transactionType'] == 'MPAY') {
            $arrData = array($this->branch_id, $this->customer_id, $this->transaction_Type, $this->transaction_Date, $this->product_id,$this->shipexheadid, $this->particulars_Details, $this->account_head, $this->bank_id, $this->voucher_no, $this->transaction_for, $this->transaction_mode, $this->received_to, $this->cheque_no, $this->cheque_date, $this->remarks, $this->amount_Total, $this->inWords, $this->modified_Date,);
            $sql = "INSERT into salestransaction(branchid,customerId,transactionType,transactionDate,product_id,shipexheadid,particulars,accountheadid,bankid,voucherNo,transactionFor,transactionMode,receivedTo,chequeNo,chequeDate,remarks,amountOut,inWords,created) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        if ($_POST['transactionType'] == 'MREC') {
            $arrData = array($this->branch_id, $this->customer_id, $this->transaction_Type, $this->transaction_Date, $this->product_id,$this->shipexheadid, $this->particulars_Details, $this->account_head, $this->bank_id, $this->crvoucher_no, $this->challan_no, $this->transaction_for, $this->transaction_mode, $this->received_from, $this->cheque_no, $this->cheque_date, $this->remarks, $this->amount_Total, $this->inWords, $this->modified_Date,);
            $sql = "INSERT into salestransaction(branchid,customerId,transactionType,transactionDate,product_id,shipexheadid,particulars,accountheadid,bankid,crvoucher,challanno,transactionFor,transactionMode,receivedFrom,chequeNo,chequeDate,remarks,amountIn,inWords,created) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        //echo "<pre>"; var_dump($arrData);echo "</pre>"; die();

        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result){
            if($_POST['transactionType']=='MREC' && $_POST['accheadId']==24) {
                $lastId = $this->lastSalesId();
                $objToArray= json_decode(json_encode($lastId), True);
                $getLastId=$objToArray['0']['id'];
                $this->lastSalesId=$getLastId;
                //Message::message("Success! Data Has Been Inserted Successfully :)");
                $this->storesales();
            }
            Message::message("Success! Data Has Been Inserted Successfully :)");
       }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');

    }

    public function index(){
/*
        $sql="SELECT X.id, products.product_name, customers.name, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X.soft_deleted ='No' GROUP BY X .id ";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
*/
return true;
    }
    public function edit(){

        $sql="SELECT  salestransaction.`id`, salestransaction.`branchid`, salestransaction.`customerId`, salestransaction.`transactionType`, salestransaction.`transactionDate`, salestransaction.`product_id`, salestransaction.`particulars`, salestransaction.`accountheadid`, salestransaction.`bankid`, salestransaction.`receiptInvoiceNo`, salestransaction.`voucherNo`, salestransaction.`crvoucher`, salestransaction.`challanno`, salestransaction.`transactionFor`, salestransaction.`transactionMode`, salestransaction.`receivedTo`, salestransaction.`receivedFrom`, salestransaction.`chequeNo`, salestransaction.`chequeDate`, salestransaction.`remarks`, salestransaction.`amountOut`, salestransaction.`amountIn`, salestransaction.`inWords`, salestransaction.`modified`,sales.id as slid, sales.`salesid`,sales.`tonweight`, sales.`kgweight` FROM salestransaction LEFT JOIN sales on salestransaction.id= sales.salesid WHERE salestransaction.id=$this->id";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function transactionEdit(){

        $sql="SELECT  * from salestransaction WHERE salestransaction.id=$this->id";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }

    public function allparties(){

        $sql = "select * from customers where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allClients(){

        $sql = "select * from party where soft_deleted='No' ORDER BY partyname ASC";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allparticulars(){

        $sql = "select * from products where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function statementTotal(){

        $sql = "select sl, sum(amount)  as totalamount  from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql="select sl, sum(amount) as totalamount from expenseincome where soft_deleted='No' AND transactionType =1 AND transactionDate BETWEEN '2015-01-01' AND '2017-01-01'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function view(){
        $sql = "SELECT salestransaction.id,products.id as productid, products.product_name,customers.id as customerid, customers.name, salestransaction.rate,salestransaction.bandwidth,salestransaction.fromDurationDate,salestransaction.toDurationDate,salestransaction.totaldays,salestransaction.amountOut,salestransaction.amountIn,salestransaction.receiptInvoiceNo,salestransaction.particulars FROM salestransaction INNER JOIN products ON salestransaction.product_id=products.id INNER JOIN customers ON salestransaction.customerId=customers.id where salestransaction.soft_deleted='No' AND salestransaction.id =$this->id";
        //$sql = "select * from expenseincome where sl=3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function trashed(){

        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function update(){

        //$arrData = array($this->book_name,$this->author_name);
       // $sql = "UPDATE  expenseincome SET particulars=?,amount=? WHERE sl=".$this->sl;

        $arrData = array($this->customer_id,$this->modified_Date,$this->product_id,$this->product_rate,$this->bandwidth_quantity,$this->from_DurationDate,$this->to_DurationDate,$this->total_Date,$this->amount_Total,$this->receipt_Invoice);
        $sql = "UPDATE salestransaction SET customerId=?,modified=?,product_id=?,rate=?, bandwidth=?,fromDurationDate=?, toDurationDate=?, totaldays=?,amount=?, receiptInvoiceNo=? WHERE id=".$this->id;


        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');
    }
    public function updateSales(){
        //var_dump($_POST); die();
        $arrData="";
        $sql="";
        if($_POST['transactionType']=='MREC' && $_POST['accheadId']==24){
            $arrData = array($this->branch_id,$this->id,$this->customer_id,$this->transaction_Type,$this->transaction_Date,$this->product_id,$this->particulars_Details,$this->account_head,$this->bank_id,$this->crvoucher_no,$this->challan_no,$this->transaction_for,$this->transaction_mode,$this->received_from,$this->cheque_no,$this->cheque_date,$this->ton,$this->kg,$this->modified_Date);
            $sql = "UPDATE sales SET branchid=?,salesid=?,customerId=?,transactionType=?,transactionDate=?,product_id=?,particulars=?,accountheadid=?,bankid=?,crvoucher=?,challanno=?,transactionFor=?,transactionMode=?,receivedFrom=?,chequeNo=?,chequeDate=?,tonweight=?,kgweight=?,modified=? WHERE salesid=".$this->id;
        }
        //echo "<pre>"; var_dump($arrData);echo "</pre>"; die();

        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if($result)
            //$this->store();
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');
    }
    public function updateTransaction()
    {
        $arrData = "";
        $sql = "";
        if ($_POST['transactionType'] == 'MPAY') {
            $arrData = array($this->branch_id, $this->customer_id, $this->transaction_Type, $this->transaction_Date, $this->product_id,$this->shipexheadid, $this->particulars_Details, $this->account_head, $this->bank_id, $this->voucher_no, $this->transaction_for, $this->transaction_mode, $this->received_to,$this->received_from, $this->cheque_no, $this->cheque_date, $this->remarks, $this->amount_Total, $this->inWords, $this->modified_Date);
            $sql = "UPDATE salestransaction SET branchid=?,customerId=?,transactionType=?,transactionDate=?,product_id=?,shipexheadid=?,particulars=?,accountheadid=?,bankid=?,voucherNo=?,transactionFor=?,transactionMode=?,receivedTo=?,receivedFrom=?,chequeNo=?,chequeDate=?,remarks=?,amountOut=?,inWords=?,modified=? WHERE id=".$this->id;
        }
        if ($_POST['transactionType'] == 'MREC') {
            $arrData = array($this->branch_id, $this->customer_id, $this->transaction_Type, $this->transaction_Date, $this->product_id, $this->particulars_Details, $this->account_head, $this->bank_id, $this->crvoucher_no, $this->challan_no, $this->transaction_for, $this->transaction_mode,$this->received_to, $this->received_from, $this->cheque_no, $this->cheque_date, $this->remarks, $this->amount_Total, $this->inWords, $this->modified_Date);
            $sql = "UPDATE salestransaction SET branchid=?,customerId=?,transactionType=?,transactionDate=?,product_id=?,particulars=?,accountheadid=?,bankid=?,crvoucher=?,challanno=?,transactionFor=?,transactionMode=?,receivedTo=?,receivedFrom=?,chequeNo=?,chequeDate=?,remarks=?,amountIn=?,inWords=?,modified=? WHERE id=".$this->id;
        }
        //echo "<pre>"; var_dump($arrData);echo "</pre>"; die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if ($result){
            if($_POST['transactionType']=='MREC' && $_POST['accheadId']==24) {
                $this->updateSales();
            }
            Message::message("Success! Data Has Been Updated  Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");
        Utility::redirect('index.php');
    }
    public function trash(){


        $sql = "UPDATE  salesentry SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function recover(){

        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");
        Utility::redirect('index.php');
    }
    public function delete(){

        $sql = "Delete from salesentry  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        }catch (PDOException $error){

            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        }catch (PDOException $error){

            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes'";
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  salestransaction SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;
        }
        if($result){
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        }
        else{
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");
            // Utility::redirect('trashed.php?Page=1');
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }
    public function recoverMultiple($markArray){

    foreach($markArray as $id){
        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$id;
        $result = $this->DBH->exec($sql);
        if(!$result) break;
    }

    if($result)
        Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
    else
        Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");
    Utility::redirect('index.php?Page=1');

    }
    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "Delete from expenseincome  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('index.php?Page=1');
    }
    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from expenseincome  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

    // start of search()
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND (product_name LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%')GROUP BY X .id ";

        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND product_name LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND x.receiptInvoiceNo LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) || !isset($requestArray['byAuthor']) && isset($requestArray['id']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND X.id='".$requestArray['search']."' GROUP BY X .id";

        if(isset($requestArray['advanced']))
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No'  AND (product_name LIKE '%".$requestArray['search']."%' OR X.receiptInvoiceNo LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%' OR X.voucherNo LIKE '%".$requestArray['search']."%' OR X.remarks LIKE '%".$requestArray['search']."%'  ) GROUP BY X .id ";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//
    // end of search()

    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        // for each search field (particulars) block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->product_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->product_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (particulars) block end

           //for each search field () block start
        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (name)  block end


        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receiptInvoiceNo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receiptInvoiceNo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (receiptInvoiceNo)  block end

        // for each search field (sl)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (sl)  block end

        // for each search field (PayerPayee)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receivedTo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receivedTo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (PayerPayee)  block end

        // for each search field (transactionId)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (transactionId)  block end
        // for each search field (remarks)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->remarks);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->remarks);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (remarks)  block end

        return array_unique($_allKeywords);

    }// get all keywords



}