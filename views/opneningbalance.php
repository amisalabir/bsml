<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction = new \App\Bsml\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$transactionData = $objTransaction->opneningbalance();
//$allData =$objTransaction->setData($_GET);
//$transactionData = $objTransaction->statement();
//var_dump($_GET); die();
/*
if ($singleUser->role=='admin') {

    if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '1' || $_GET['branchid'] == '2' || $_GET['branchid'] == '3') )
        $transactionData = $objTransaction->opneningbalance();
    if ($_GET['bookname'] == 'CASH' && $_GET['branchid'] == 'all')
        $transactionData = $objTransaction->opneningbalance();
}
else{
    if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '2' || $_GET['branchid'] == '3'))
        $branch="(Yard)";
    $transactionData = $objTransaction->opneningbalance();
}
/*Branch selection*/
/*
if($_GET['branchid']=='1') $branch="(Head Office)";
if($_GET['branchid']=='2') $branch="(Yard)";
if($_GET['branchid']=='3') $branch="(Petty Cash-Yard)";
if($_GET['branchid']=='all') $branch="(All Branch)";
*/
//echo "<pre>";var_dump($transactionData); echo "</pre>"; die();
################## statement  block start ####################
/*
if(isset($_REQUEST['fromTransaction']) ) {
    //$someData = $objBookTitle->statement($_REQUEST);
    //$serial = 1;

      echo "<pre>"; var_dump($someData); echo "</pre>"; die();

    $allData =$objTransaction->setData($_GET);
    $allData = $objTransaction->statement();
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
/*
    if(($_REQUEST['customerId'])=='all') {
        $allData = $objTransaction->allStatement();
        //echo "<pre>"; var_dump($someData); echo "</pre>"; die();
    }
    if(($_REQUEST['customerId'])!=='all') {
        $allData = $objTransaction->statement();
        $_SESSION['someData'] = $allData;
    }

    $_SESSION['someData']=$allData;
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
    Converting Object to an Array
    $objToArray = json_decode(json_encode($allData), True);
    $customeName=$objToArray['0']['name'];
    $statementTotal=$objBookTitle->statementTotal();

}*/
$_SESSION['someData']=$transactionData;
//echo "<pre>"; var_dump($allData); echo "</pre>"; die();
//Converting Object to an Array
$objToArray = json_decode(json_encode($transactionData), True);
//cho "<pre>"; var_dump($objToArray); echo "</pre>"; die();

$customeName=$objToArray['0']['headnameenglish'];


//$statementTotal=$objBookTitle->statementTotal();
$serial = 1;
################## statement  block end ######################


include_once ('header.php');
include_once('printscript.php');
?>
<div align="center" class="content">
    <div align="center" class="container ctn">
        <div align="center" class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <!-- required for search, block 4 of 5 start -->
        <!-- Search nav start-->
        <?php // include_once ('searchnav.php'); ?>
        <!-- Search nav ended-->
        <!-- Date selection ended -->
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
            <span><br><br> </span>
            <div class="container">
                <div id="dvContainer">
                    <style>
                        <?php
                        include ('../resource/css/printsetup.css')
                        ?>
                    </style>
                    <table >
                        <thead>
                        <tr>
                            <td colspan="3" align="center" >
                                <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Ship Breakers Ltd.</font> <br>
                                <font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
                                <font style="font-size:13px">(<?php echo "Statement Since : 01-01-2018 to 30-05-2018";?>)</font>
                            </td>
                        </tr>
                        <tr><td ><b><?php  echo "HEAD: Opening Balance :All"; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
                        </thead>
                        <tr> <td colspan="3">
                                <!-- Inner Table -->
                                <div class="row" align="center">
                                    <div id="reporttable" class="col-sm-12 text-center" align="center" >
                                        <table width="auto"   class="" >
                                            <thead>
                                            <tr style="background-color:#F2F2F2;">
                                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                                <th class="text-center">SL</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center" width="500px">Description</th>
                                                <th class="text-center">Voucher/ <br>Challan No</th>
                                                <th class="text-center">Received <br> (Taka)</th>
                                                <th class="text-center">Payment <br> (Taka)</th>
                                                <th class="text-center">Balance</th>
                                            </tr>
                                            </thead>
                                            <?php
                                            $totalAmountIn=0;
                                            $totalAmountOut=0;
                                            $balance=0;
                                            //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
                                            // echo "<pre>"; var_dump($allData); echo "</pre>"; die();
                                            foreach($transactionData as $oneData){ ########### Traversing $someData is Required for pagination  #############
                                                //$totalDays=abs('$oneData->toDurationDate','$oneData->fromDurationDate');
                                                if($serial%2) $bgColor = "AZURE";
                                                else $bgColor = "#ffffff";
                                                $totalAmountIn=$totalAmountIn+$oneData->amountIn;
                                                $totalAmountOut=$totalAmountOut+$oneData->amountOut;
                                                //$totalAmount=$totalAmount+$oneData->amount ;
                                                $balance=($balance-$oneData->amountOut)+$oneData->amountIn;
                                                $voucherType=""; $voucherOrChallan="";
                                                if($oneData->voucherNo!=Null){$voucherType="Dr -"; $voucherOrChallan=$oneData->voucherNo;}else{$voucherType="Cr -"; $voucherOrChallan=$oneData->crvoucher;}
                                                if($oneData->challanno!=Null ||($oneData->voucherNo==Null && $oneData->crvoucher==Null )){$voucherType="Ch -"; $voucherOrChallan=$oneData->challanno;}
                                                echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:5px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>";
                                                if($singleUser->role=='admin'){ echo "<a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>$serial</a>";} else{echo $serial;}
                                                echo"</td>
                      <td class='text-center'>$oneData->transactionDate</td>
                     <td class='text-left'> $oneData->headnameenglish: $oneData->accountname $oneData->product_name $oneData->partyname<br> $oneData->transactionFor $oneData->remarks  </td>
                     <td class='text-center'>$voucherType $voucherOrChallan</td>
                     <td style='text-align: right;'>".number_format($oneData->amountIn,0)."</td>
                     <td style='text-align: right;'>".number_format($oneData->amountOut,0)."</td>
                     <td class='text-right'>".number_format($balance,0)."</td>                     
                     <!--
                     <td class='text-center'>
                       <div class='dropdown text center' style='background-color: inherit;'>
                      <button style='background-color: inherit;' class=' dropdown-toggle' type='button' id='menu1' data-toggle='dropdown'>Actions
                            <span class='caret'></span></button>
                        <ul style='background-color:#fcfcfc;' class='dropdown-menu' role='menu' aria-labelledby='menu1'>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='view.php?id=$oneData->id'>View</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>Edit</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='trash.php?id=$oneData->id'>Trash</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='delete.php?id=$oneData->id'>Delete</a></li>           
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='email.php?id=$oneData->id'>Email</a></li>           
                        </ul>
                        </div>                        
                     </td>
                     -->
                  </tr>
              ";
                                                $serial++; }
                                            echo "     
                        <tr style='background-color:; font-weight: bold;'>
                            <td style='text-align: right;' colspan='5'> Total Taka: <span>&nbsp;&nbsp; </span></td>
                            <td class='text-right'>".number_format($totalAmountIn,0)." </td>
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='tablefooter'>".number_format($balance,0)." </td>
                        </tr>
                        "; ?>
                                        </table>
                                        <br>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
