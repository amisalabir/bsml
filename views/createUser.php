<?php

//ini_set('MAX_EXECUTION_TIME', -1);
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################
//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction= new \App\Bsml\Transaction();
$allData = $objTransaction->statement();
$allClients=$objBookTitle->allClients();
$accountHead=$objTransaction->accounthead();
$allparticulars=$objBookTitle->allparticulars();

$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form action="#" name="addUser" class="signleTranscation" method="post">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Create User</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Name</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="userName" required>
                                    <input type="hidden"  class="form-control" name="addUser" value="addUser" required>
                                </td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td><input type="email" class="form-control" name="email" required></td>
							</tr>
							<tr>
								<td>Branch</td>
								<td>:</td>
								<td>
									<select name="branch" class="form-control" required>
										  <option value="Chittagong" selected>Chittagong</option>
										  <option value="Dhaka">Dhaka</option>
										  <option value="Comilla">Comilla</option>
										</select>
								</td>
							</tr>
							<tr>
								<td>User Type</td>
								<td>:</td>
								<td>
									<select name="userType" class="form-control" required>
										  <option value="User" selected>User</option>
										  <option value="Admin">Admin</option>
										</select>
								</td>
							</tr>
							<tr>
								<td>Password</td>
								<td>:</td>
								<td><input type="password" class="form-control" name="password" required></td>
							</tr>
							<tr>
								<td>Confirm Password</td>
								<td>:</td>
								<td><input type="password" class="form-control" name="confirmPass" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>

<?php
include ('footer.php');
include ('footer_script.php');
?>