<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction = new \App\Bsml\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);
//var_dump($_GET); die();



if ($_GET['bookname']=='LEDGER' && ($_GET['branchid']=='1' || $_GET['branchid']=='2' ))
    $transactionData = $objTransaction->singleledger();
if($_GET['bookname']=='LEDGER' && $_GET['branchid']=='all')
    $transactionData = $objTransaction->ledger();

################## statement  block start ####################
/*Branch selection*/
    if($_GET['branchid']=='1') $branch="(Head Office)";
    if($_GET['branchid']=='2') $branch="(Yard)";
    if($_GET['branchid']=='all') $branch="(All Branch)";

    $_SESSION['someData']=$transactionData;
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
    //Converting Object to an Array
    $objToArray = json_decode(json_encode($transactionData), True);
   //cho "<pre>"; var_dump($objToArray); echo "</pre>"; die();
    $customeName="ALL";
    if($_GET['bookname']=='LEDGER'){$customeName=$objToArray['0']['headnameenglish'];}
    //$statementTotal=$objBookTitle->statementTotal();
    $serial = 1;
################## statement  block end ######################

include ('header.php');
include_once ('printscript.php');?>
<div align="center" class="content">
    <div class="container ctn">
 <div align="center" class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<!-- required for search, block 4 of 5 start -->
<!-- Search nav start-->
<?php // include_once ('searchnav.php'); ?>
<!-- Search nav ended-->
<!-- Date selection ended -->
    <form action="trashmultiple.php" method="post" id="multiple">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>

                </div>
                </div>
                <div class="col-md-1"></div>
            </div>

        <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
        <div class="container text-center " style="padding: 0 0 5px 0;" >
            <h1> <?php
                /*if(($_REQUEST['customerId'])=='all') {
                echo "All Orders";} else{ echo $customeName;}
*/
                ?> </h1>
        </div>
        <div class="container">
            <div id="dvContainer" align="center">
                <style>
                <?php
                include ('../resource/css/printsetup.css')
                ?>
                </style>

                <table id="outerTable" >
                    <thead>
                    <tr>
                        <td colspan="3" align="center" >
                            <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Steel Mills Ltd.</font> <br>
                            <font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
                            <font style="font-size:13px">(<?php echo "Statement Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?>)</font>
                        </td>
                    </tr>
                    <tr><td ><b><?php  echo "HEAD:".$customeName.$branch; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
                    </thead>
                    <tr> <td colspan="3">
                            <!-- Inner Table -->
                            <div class="row" align="center">
                                <div id="reporttable" class="col-sm-12 text-center" align="center" >
                      <table id="innerTable" width="100%"  >
                          <thead>
                            <tr style="background-color:#F2F2F2;">
                            <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                            <th class="text-center">SL</th>
                            <th class="text-center">Date</th>
                           <th class="text-center" width="300px">Description</th>
                            <th class="text-center">Voucher No</th>
                            <th class="text-center">DR (Taka)</th>
                           <th class="text-center">CR (Taka)</th>
                            <th class="text-center"> Balance</th>
                        </tr>
                          </thead>
                        <?php

                            $totalAmountIn=0;
                            $totalAmountOut=0;
                            $balance=0;

                        if($_GET['accheadId']==69){
                            $transactionData = $objTransaction->headWiseStatement();
                            include 'ledger/cashledger.php';
                        }
                        else{

                            include 'ledger/allheadledger.php';
                        }

                        ?>

                    </table>
                    <br>
                </div>

            </div>
              </td>
          </tr>
          </table>
            </div>
        </div>
  </form>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
