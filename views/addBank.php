<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction= new \App\Bsml\Transaction();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

include ('header.php');
?>
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">

    </script>
    <form class="form-group" name="BankEntry" action="store.php" method="post">
        <input hidden name="addBank" type="text" value="addBank">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="bankname">BANK NAME :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="bankname" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="accountname">ACCOUNT NAME:</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="accountname" required type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="accountnumber">ACCOUNT NUMBER :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="accountnumber" required type="text">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="branch">BRANCH :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="branch" required type="text">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="address">ADDRESS :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="address" required type="text">
                    </div>
                    <div class="col-sm-4"></div>
                </div>

            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-2 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
