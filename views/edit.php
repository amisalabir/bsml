<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
}
################################ End of Session time calculation ##############################

################################ Admin permission ##############################
if($singleUser->role!=='admin') {
    Utility::redirect('index.php');
}

################################ Admin permission ##############################

$objBookTitle = new \App\Bsml\Bsml();
$objTransaction= new \App\Bsml\Transaction();
$objBranch=new \App\Bsml\Branch();
$objBookTitle->setData($_GET);
$allData = $objBookTitle->index();
$editData = $objBookTitle->transactionEdit();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$shipexhead=$objTransaction->shipexhead();
$bankNme=$objTransaction->allbank();
$products=$objTransaction->prducts();
$branches=$objBranch->branch();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

################## object to array ##################
/*Converting Object to an Array*/
$objToArray = json_decode(json_encode($editData), True);
$objToArrayShipex = json_decode(json_encode($shipexhead), True);

if($objToArray['0']['accountheadid']=='24'){
    $objBookTitle->setData($_GET);
    $transactionEditData = $objBookTitle->edit();
    $objToArray = json_decode(json_encode($transactionEditData), True);
}
################## object to array ##################
include_once ('header.php');
//echo"<pre>";var_dump($singleUser); echo"</pre>";
?>
<script type="text/javascript">
    <?php
    //include '../resource/js/edit.js';
    include '../resource/js/addtransaction.js';
    ?>
</script>

<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <form class="form-group" name="transactionEntry" action="update.php" method="post">
        <input hidden name="updateTransaction" type="text" value="updateTransaction">
        <input hidden name="transactionid" type="text" value="<?php echo $objToArray['0']['id'];?>">
        <input name="modifiedDate" type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">Transaction Type :</label> </div>
                    <div class="col-sm-4 text-left">
                        <select id="transactionType" name="transactionType" onchange="checkTransactionType(this.value)" style="width:150px; " class="form-control">
                           <?php
                           $selected='';
                           if($objToArray['0']['transactionType']=='MREC')
                           {$selected='<option   value="MPAY">PAYMENT</option><option selected    value="MREC">RECEIPT</option>';}
                           if($objToArray['0']['transactionType']=='MPAY'){
                                   $selected='<option selected value="MPAY">PAYMENT</option><option   value="MREC">RECEIPT</option>';
                           }
                           if($objToArray['0']['transactionType']=='OB'){
                               $selected='<option value="MPAY">PAYMENT</option><option   value="MREC">RECEIPT</option>';

                           }
                           echo $selected;
                           ?>
                            <!--
                            <option value="JOUR">JOURNAL</option>
                            <option value="CONT">CONTRA</option>
                            -->
                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Related Client / Party :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select name="customerId" style="width:150px; " class="form-control text-uppercase ">
                            <option class='text-uppercase' selected value='0'> N/A</option>
                            <?php
                            foreach ($allClients as $individualClient){
                                if($objToArray['0']['customerId']==$individualClient->id){
                                    echo  "<option class='text-uppercase' selected value='$individualClient->id'> $individualClient->partyname</option>";
                                }
                                else{
                                    echo  "<option class='text-uppercase' value='$individualClient->id'> $individualClient->partyname</option>";
                                }
                            }
                            ?>
                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Account Head :</label> </div>
                    <div class="col-sm-3 text-left ">
                     <select id="accheadId" name="accheadId" onchange="CheckAccountHeadField(this.value)"    class="form-control text-uppercase ">
                         <option class='' value='0'>N/A</option>
                         <?php
                      foreach ($accountHead as $singleHead){
                         if($objToArray['0']['accountheadid']==$singleHead->id) {
                             echo "<option class='' selected value='$singleHead->id'>$singleHead->headnameenglish ($singleHead->id)</option>";
                         }else{
                             echo "<option class='' value='$singleHead->id'>$singleHead->headnameenglish ($singleHead->id)</option>";
                         }
                      } ?>
                     </select>
                    </div>
                  <div id="productId" class="col-sm-3">

                      <?php
                      if($objToArray['0']['accountheadid']=='24' || $objToArray['0']['accountheadid']=='61') {
                          echo "<select id='shipid'  name='shipid'  class='form-control text-uppercase'><option class='' value='0'>SELECT ONE</option>";

                          foreach ($products as $singleproducts) {
                              echo "<option ";
                              if($singleproducts->id==$objToArray['0']['product_id']){echo "selected";}
                              echo " value=".$singleproducts->id . ">" . $singleproducts->product_name . "</option>";                   }



                          echo "</select>";
                      }
                      ?>

                  </div>
                  <div class="col-sm-2"></div>
                </div>
                <div id="weight" style='display:;'>
                    <?php
                    if($objToArray['0']['accountheadid']=='24'){
                        echo "<div class=\"row\" ><div class=\"col-sm-4 text-right form-group\"><label for=\"weight\">Weight :</label></div><div class=\"col-sm-2 text-left\"><input class=\"form-control\"  name=\"tonWeight\" value=\"".$objToArray['0']['tonweight']."\" type=\"text\" > TON</div><div class=\"col-sm-1\"><input class=\"form-control\"  name=\"kgWeight\" value=\"".$objToArray['0']['kgweight']."\" type=\"text\" > KG</div> <div class=\"col-sm-5\"></div></div>";
                    }
                    if($objToArray['0']['accountheadid']=='61'){
                        echo "<div class=\"row\" ><div class=\"col-sm-4 text-right form-group\"><label for=\"weight\">Ship Expense Head :</label></div> <div class=\"col-sm-5\"><select class=\"form-control text-uppercase\"  name=\"shipexheadid\" id=\"shipexheadid\"><option class='' value='0'>SELECT ONE</option>";

                        foreach ($shipexhead as $singleShipex) {
                            echo "<option ";
                            if($singleShipex->id==$objToArray['0']['shipexheadid']){echo "selected";}
                            echo " value=".$singleShipex->id . ">" . $singleShipex->name . "</option>";
                        }

                        echo"</select><div class=\"col-sm-2 text-left\">  </div>  </div></div>";
                    }

                    ?>
                </div>
                <div id="activeBankField"   style='display:none;'>
                    <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedBank">Bank Name :</label> </div>
                    <div class="col-sm-5 text-left ">
                        <select id="bankId"  name="bankId"  class="form-control text-uppercase ">
                            <option class="text-uppercase" value="0">SELECT BANK</option>
                      <?php
                            foreach ($bankNme as $singlBank){ echo  "<option class='text-uppercase' value='$singlBank->id'> $singlBank->bankname ($singlBank->accountname) </option>";}
                      ?>
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Branch :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="branchid" style="width:auto; " class="form-control text-uppercase ">
                            <?php
                            foreach ($branches as $branch){

                                echo "<option value='$branch->id'";
                                if ($branch->id==$objToArray['0']['branchid']) {echo 'selected';}
                                echo">$branch->branchname</option>";
                            }
                            ?>

                        </select></div>

                    <div class="col-sm-2"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionDate">Transaction Date :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control selectDate" id="transactionDate" name="transactionDate" required  value="<?php echo $objToArray['0']['transactionDate'];  ?>" onchange="" onkeypress="" type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div id="voucherNo"  class="row" style="display:block;">
  <?php $voucher='';
  if(!is_null($objToArray['0']['voucherNo'])){
      $voucher="<div class=\"col-sm-4 text-right form-group\"><label for=\"voucherNo\"> Voucher No (DR) :</label></div><div class=\"col-sm-2 text-left\"><input class=\"form-control\" value=".$objToArray['0']['voucherNo']." name=\"voucherNo\" type=\"text\" ></div><div class=\"col-sm-6\"></div>";}
  if(!is_null($objToArray['0']['crvoucher'])){
      $voucher="<div class=\"col-sm-4 text-right form-group\"><label for=\"crVoucherNo\"> Voucher No (CR) :</label></div><div class=\"col-sm-2 text-left\"><input value=".$objToArray['0']['crvoucher']." class=\"form-control \" name=\"crVoucherNo\" type=\"text\" ></div><div class=\"col-sm-6\"></div>"; }
  if(!is_null($objToArray['0']['challanno'])){
     $voucher="<div class=\"col-sm-4 text-right form-group\"><label for=\"challanNo\"> Challan No :</label></div><div class=\"col-sm-2 text-left\"><input value=".$objToArray['0']['challanno']." class=\"form-control\" name=\"challanNo\" type=\"text\" ></div><div class=\"col-sm-6\"></div>
";
  }

      echo $voucher;
                    ?>
                </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionFor">  Transaction For :</label></div>
                    <div class="col-sm-3 text-left"><input class="form-control" name="transactionFor" <?php echo 'value="'. $objToArray['0']['transactionFor'].'"'; ?>   type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionMode">Transaction Mode :</label></div>
                    <div class="col-sm-2 text-left">
                        <select id="transactionMode" name="transactionMode" class="form-control" onchange='ChequeField(this.value);' required>
                            <option <?php if($objToArray['0']['transactionMode']=='CASH'){echo 'selected';}  ?>  value="CASH">CASH</option>
                            <option <?php if($objToArray['0']['transactionMode']=='CASH CHEQUE'){echo 'selected';}  ?>  value="CASH CHEQUE">CASH CHEQUE</option>
                            <option <?php if($objToArray['0']['transactionMode']=='A/C PAYEE CHEQUE'){echo 'selected';}  ?>value="A/C PAYEE CHEQUE">A/C PAYEE CHEQUE</option>
                            <option <?php if($objToArray['0']['transactionMode']=='ONLINE TRANSFER'){echo 'selected';}  ?>value="ONLINE TRANSFER">ONLINE TRANSFER</option>
                            <option <?php if($objToArray['0']['transactionMode']=='MOBILE BANK'){echo 'selected';}  ?>value="MOBILE BANK">MOBILE BANK</option>
                            <option <?php if($objToArray['0']['transactionMode']=='PAY ORDER'){echo 'selected';}  ?>value="PAY ORDER">PAY ORDER</option>
                            <option <?php if($objToArray['0']['transactionMode']=='ATM'){echo 'selected';}  ?>value="ATM">ATM</option>
                            <option <?php if($objToArray['0']['transactionMode']=='D.D.'){echo 'selected';}  ?>value="D.D.">D.D.</option>
                            <option <?php if($objToArray['0']['transactionMode']=='T.T.'){echo 'selected';}  ?>value="T.T.">T.T.</option>
                            <option <?php if($objToArray['0']['transactionMode']=='OTHERS'){echo 'selected';}  ?>value="OTHERS">OTHERS</option>
                        </select>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row" id="paidTo"  >
                    <?php $paid='';
                    if(!is_null($objToArray['0']['receivedTo'])){
                        $paid="<div class=\"col-sm-4 text-right form-group\"><label for=\"receivedTo\">Paid To :</label></div><div class=\"col-sm-3 text-left\"><input class=\"form-control\" name=\"receivedTo\" value=\"".$objToArray['0']['receivedTo']."\" type='text' ></div><div class=\"col-sm-5\"></div>";}
                    if(!is_null($objToArray['0']['receivedFrom'])){
                        $paid="<div class=\"col-sm-4 text-right form-group\"><label for=\"receivedFrom\">Received From :</label></div><div class=\"col-sm-3 text-left\"><input class=\"form-control\" name=\"receivedFrom\" value=\"".$objToArray['0']['receivedFrom']."\" type='text' ></div><div class=\"col-sm-5\"></div>";}
                    echo $paid;
                        ?>
                </div>
                <div id="activeChequeField" style='display:;'>
                <?php
                if(!is_null($objToArray[0]['chequeNo'])){
                echo "<div class=\"row\" ><div class=\"col-sm-4 text-right form-group\"><label for=\"chequeNo\">Cheque No :</label></div><div class=\"col-sm-3 text-left\"><input class=\"form-control\"  name=\"chequeNo\" value=".$objToArray[0]['chequeNo']."  type=\"text\" ></div><div class=\"col-sm-5\"></div></div><div class=\"row\"><div class=\"col-sm-4 text-right form-group\"><label for=\"chequeDate\"> Cheque Date :</label></div><div class=\"col-sm-3 text-left\"><input class=\"form-control selectDate\"  name=\"chequeDate\" id=\"chequeDate\" value=\"".$objToArray[0]['chequeDate']."\"  type=\"text\" ></div><div class=\"col-sm-5\"></div></div>";
                }
                ?>

                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> Remarks :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control" name="remarks" rows="2" cols="20"><?php echo $objToArray[0]['remarks'];?></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> Amount :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="amount" value="<?php echo $objToArray[0]['amountOut']+$objToArray[0]['amountIn'];?>"  id="txtAmount" required type="text">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="inWords">In Words :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control" name="inWords"><?php echo $objToArray[0]['inWords'];?>
                        </textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
