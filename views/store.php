<?php
require_once("../vendor/autoload.php");
if(!isset($_SESSION) ) session_start();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
use \App\Bsml\Bsml;
use App\Bsml\Transaction;
use \App\Customer\Customer;
use \App\Bsml\Vessel;
use \App\Bsml\Head;
use \App\Bsml\Bank;


$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(!$status) {     Utility::redirect('index.php');  return; }
############################### Session time calculation #####################################


if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################
 if(isset($_POST['addOrder'])){
    $objExpenseIncome = new Bsml();
    $objExpenseIncome->setData($_POST);
    $objExpenseIncome->store();

}
if(isset($_POST['addCustomer'])) {
    //var_dump($_POST); die();
    $objCustomer = new Customer();
    $objCustomer->setData($_POST);
    $objCustomer->store();
}
if(isset($_POST['addTransaction'])) {
     //echo "<pre>"; var_dump($_POST);echo "</pre>"; die();
    $objTransaction=new Bsml();
    $objTransaction->setData($_POST);
    if($_POST['accheadId']=='24'){
        $objTransaction->store();
        $objTransaction->storesales();
    }else{
        $objTransaction->store();
    }

}
if(isset($_POST['addVessel'])) {
    //var_dump($_POST); die();
    $objVessel=new Vessel();
    $objVessel->setData($_POST);
    $objVessel->store();
}
if(isset($_POST['addHead'])) {

    $objVessel=new Head();
    $objVessel->setData($_POST);
    $objVessel->store();
}
if(isset($_POST['addBank'])) {
    $objVessel=new Bank();
    $objVessel->setData($_POST);
    $objVessel->store();
}
if(isset($_POST['addParty'])) {
    $objVessel=new \App\Bsml\Party();
    $objVessel->setData($_POST);
    $objVessel->store();
}