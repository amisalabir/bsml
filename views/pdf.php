<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();
use App\Bsml\Bsml;
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

$obj= new Bsml();

##################### Checking Page Name started ########################
$url = $_SERVER['HTTP_REFERER'];
preg_match('/\/[a-z0-9]+.php/', $url, $match);
$page = array_shift($match);
##################### Checking Page Name ended   ########################

if($page=='/customerlist.php'){

    //$allData=$_SESSION['allClients'];
    $recordSet=$_SESSION['allClients'];

/*
    echo "<pre>";
    var_dump($recordSet);
    echo "<pre>";
    die();
*/
    $trs="";
    $sl=0;
    foreach($recordSet as $row) {
        $id = $row->id;
        $clientName = $row->name;
        $email = $row->email;
        $phone=$row->phone;
        $address=$row->address;
        $sl++;
        $trs .= "<tr>";
        $trs .= "<td > $id </td>";
        $trs .= "<td > $clientName </td>";
        $trs .= "<td > $email </td>";
        $trs .= "<td >$phone </td>";
        $trs .= "<td >$address </td>";
        $trs .= "</tr>";
    }
########################################################

########################################################
$html= <<<BITM
<div class="table-responsive">
<style>
    table {
        border-collapse: collapse; text-align: center;
    }
    table, th, td {
        border: 1px solid black;
    }
</style>
            <div align="left" style="padding-bottom: 10px;">
            <img class=""  src="../../../resource/img/logo.png"/>
            </div>
            <table align="center" width="100%" class="table">
             <thead>
                
                <tr>
                    <th >ID</th>
                    <th >Name</th>
                    <th >Email</th>
                    <th >Phone</th>
                    <th >Address</th>
                  </tr>
                </thead>
                <tbody>
             $trs
               </tbody>
            </table>
BITM;
}

if($page=='/allorders.php'){

    $recordSet=$_SESSION['orderList'];

    //var_dump($allData);
    $trs="";
    $sl=0;
    foreach($recordSet as $row) {
        $id = $row->id;
        $clientName = $row->name;
        $productName = $row->product_name;
        $rate=$row->rate;
        $bandwidth=$row->bandwidth;
        $fromDate=$row->fromDurationDate;
        $toDate=$row->toDurationDate;
        $totalDays=$row->totaldays;
        $amount=$row->amount;
        $sl++;
        $trs .= "<tr>";
        $trs .= "<td > $id </td>";
        $trs .= "<td > $clientName </td>";
        $trs .= "<td > $productName </td>";
        $trs .= "<td > $rate </td>";
        $trs .= "<td >$bandwidth </td>";
        $trs .= "<td >$fromDate</td>";
        $trs .= "<td >$toDate</td>";
        $trs .= "<td >$totalDays</td>";
        $trs .= "<td >$amount</td>";
        $trs .= "</tr>";
    }
########################################################

########################################################
$html= <<<BITM
<div class="table-responsive">
<style>
    table {
        border-collapse: collapse; text-align: center;
    }
    table, th, td {
        border: 1px solid black;
    }
</style>
            <div align="left" style="padding-bottom: 10px;">
            <img class=""  src="../../../resource/img/logo.png"/>
            </div>
            <table align="center" width="100%" class="table">
               <thead>
                     <tr>
                    <th >ID</th>
                    <th >Name</th>
                    <th >Product</th>
                    <th >Rate</th>
                    <th >Bandwith</th>
                    <th >Start Date</th>
                    <th >End Date</th>
                    <th >Total Days</th>
                    <th >Amount</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>

BITM;
}

// Require composer autoload
require_once ('../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser

$mpdf->Output('list.pdf', 'D');