<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(!$status) {
    Utility::redirect('index.php');
    return; }

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction = new \App\Bsml\Transaction();
$objTrialBalance = new \App\Bsml\TrialBalance();
$accountHead=$objTransaction->accounthead();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);

################# Branch Selection ####################
$branch="";
if ($_GET['branchid'] == '1'){$branch="Head Office";}
if ($_GET['branchid'] == '2'){$branch="PETTY CASH (BSML)";}
if ($_GET['branchid'] == '3'){$branch="ALL BRANCHES";}


$_SESSION['branchid']=$_GET['branchid'];
$_SESSION['fromTransaction']=$_GET['fromTransaction'];
$_SESSION['toTransaction']=$_GET['toTransaction'];

################## TrialBalance  block Start ######################
include ('header.php');
include_once ('printscript.php');?>
<div align="center" class="content">
    <div class="container ctn">
 <div align="center" class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
    <form action="trashmultiple.php" method="post" id="multiple">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton;
                    } else{
                        //echo $userButton.$adminButton;
                    }

                    ?>

                </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        <div class="container text-center " style="padding: 0 0 5px 0;" >
            <h1> <?php
                /*if(($_REQUEST['customerId'])=='all') {
                echo "All Orders";} else{ echo $customeName;}
*/
                ?> </h1>
        </div>
        <div class="container">
            <div id="dvContainer" align="center">
                <style>
                <?php
                include ('../resource/css/printsetup.css')
                ?>
                </style>
                <table id="" width="780px" >
                    <thead>
                    <tr>
                        <td colspan="3" align="center" >
                            <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">Bhatiyari Steel Mills Ltd.</font> <br>
                            <font style="font-size:14px">Bhatiyari, Sitakunda, Chittagong.</font><br>
                            <font style="font-size:13px">(<?php echo "Trial Balance Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?>)</font>
                        </td>
                    </tr>
                    <tr><td ><b><?php  echo "HEAD: Trial Balance (".$branch.")"; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
                    </thead>
                    <tr> <td colspan="3">
                            <!-- Inner Table -->
                            <div class="row" align="center">
                                <div id="reporttable" class="col-sm-12 text-center" align="center" >
                      <table id="innerTable" width="780px"  >
                          <thead>
                            <tr style="background-color:#F2F2F2;">
                             <th class="text-center">SL</th>
                           <th class="text-center" width="auto">Description</th>
                           <th class="text-center" colspan="2">Debit <br>(Amount in Taka)</th>
                           <th class="text-center" colspan="2">Credit<br>(Amount in Taka)</th>
                        </tr>
                          </thead>

                        <?php
                            $totalAmountIn=0;
                            $totalAmountOut=0;
                            $balance=0;
                            $totalHeadDebitBalance=0;
                            $totalHeadCreditBalance=0;

                        if ($_GET['branchid'] == '1'){
                            include ('trialbalance/cashInHandHeadOffice.php');
                        }
                        if ($_GET['branchid'] == '2'){
                            include ('trialbalance/cashInHandYard.php');
                        }
                        if ($_GET['branchid'] == '3'){
                            include ('trialbalance/cashInHandHeadOffice.php');
                            include ('trialbalance/cashInHandYard.php');
                            include ('trialbalance/cashInHandPetty.php');
                        }
                        $serialNumber = 1;
                        foreach($accountHead as $oneData){

                            $setData['accheadId']=$oneData->id;
                            $setData['fromTransaction']=$_GET['fromTransaction'];
                            $setData['toTransaction']=$_GET['toTransaction'];
                            $setData['branchid']=$_GET['branchid'];
                            $objTransaction->setData($setData);
                            $total=$objTransaction->headTotal();
                            ################################
                            $objToArray = json_decode(json_encode($total), True);
                            ################################################
                            $heahDebitBalance=0;
                            $headCreditBalance=0;
                            if($objToArray[0]['balance']<0)
                            {$headDebitBalance=$objToArray[0]['balance']; $debitBalance=0; $totalAmountOut+=$headDebitBalance;}
                            elseif($objToArray[0]['balance']==0)
                            {$headCreditBalance=$objToArray[0]['balance']; $headDebitBalance=$objToArray[0]['balance'];}
                            else{$headCreditBalance=$objToArray[0]['balance']; $headDebitBalance=0; $totalAmountIn+=$headCreditBalance;}
                            /*
                            if($oneData->id==59){
                                include 'trialbalance/bank.php';
                            }
                            ################################################
                            elseif($oneData->id==0){

                                include 'trialbalance/party.php';
                            }
                            ################################################
                            elseif($oneData->id==24){

                               include 'trialbalance/sale.php';
                            }
                            */
                            ################################################
                              if($objToArray[0]['balance']!=0){
                                    echo "
                             <tr  >
                                <td style='text-align: center;'><a style='text-decoration: none;' href='headwisestatement.php?accheadId=$oneData->id'>$serialNumber</a></td>
                                 <td class='text-left text-uppercase'>$oneData->headnameenglish </td>
                                    <td width='80' style='text-align: right;'></td>
                                  <td  style='text-align: right;'>"; if($objToArray[0]['balance']<0){echo number_format(abs($objToArray[0]['balance']),0);} echo"</td>
                                   <td width='80' style='text-align: right;'></td>
                                <td  style='text-align: right;'> "; if($objToArray[0]['balance']>0){echo number_format(abs($objToArray[0]['balance']),0);} echo"</td>         
                            </tr>
                            ";
                                }
                         $serialNumber++; }

                  echo "     
                        <tr style='background-color:; text-align:right; height: 30px; font-size: large; font-weight: bold;'>
                            <td class='text-right' colspan='2'> Total:</td>
                            <td colspan='2' class='text-right'>".number_format(abs($totalAmountOut),0)."</td>
                            <td colspan='2'  class='text-right'>".number_format(abs($totalAmountIn),0)."</td>
                            
                        </tr>
                        "; ?>

                    </table>
                    <br>
                </div>

            </div>
              </td>
          </tr>
          </table>
            </div>
        </div>
  </form>

    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
