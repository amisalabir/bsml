<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Reference Wise - Bill Status</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Office Name</td>
								<td>:</td>
								<td><input type="text" name="OffName" class="form-control" required></td>
							</tr>
							<tr>
								<td>Job Type</td>
								<td>:</td>
								<td> <select name="jobType" class="form-control" required>
										  <option value="Export" selected>Export</option>
										  <option value="Import">Import</option>
										</select></td>
							</tr>
							<tr>
								<td>Refference Type</td>
								<td>:</td>
								<td>
									 <select name="refType" class="form-control" required>
										  <option value="contrans" selected>CONTRANS</option>
										  <option value="direct Export">DIRECT EXPORT</option>
										  <option value="direct Import">DIRECT IMPORT</option>
										  <option value="DO">DO</option>
										  <option value="F.Imp">F.IMP</option>
										  <option value="F.Exp">F.EXP</option>
										  <option value="Consol">CONSOL</option>
										</select>
								</td>
							</tr>
							<tr>
								<td>Job Year</td>
								<td>:</td>
								<td><input type="text" name="jobYear" class="form-control" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="Search" value="View Report"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 