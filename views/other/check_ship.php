<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction = new \App\Bsml\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
//if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);
$allData = $objTransaction->statement();

################## statement  block start ####################

//if(isSet($_POST['accounthead'])){
//$username = $_POST['username'];
    $productShip=$objTransaction->prductsShip();
    //var_dump($productShip); die();
    echo"<select id='shipid'  name='shipid'  class='form-control text-uppercase'>";
    foreach($productShip as $oneData){
        echo  "<option class='' value='$oneData->id'>$oneData->vesselname ($oneData->id)</option>";
    }
    echo "</select>";

//}

?>