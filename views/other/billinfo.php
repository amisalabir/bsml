<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Bill Information</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Job No & Year</td>
								<td>:</td>
								<td>
								<div class="col-auto form-inline">
									<input type="text" name="jobNo" class="form-control" required>
									<input style="width:80px;" type="text" name="jobNo2" class="form-control" required>
									<input style="width:80px;" type="text" name="jobYear" placeholder="Year" class="form-control" required>
								</div>
								</td>
							</tr>
							<tr>
								<td>Company ID</td>
								<td>:</td>
								<td>
								<div class="col-auto form-inline">
									<input style="width:120px;" type="text" name="compId" class="form-control" required>
									Date :
									<input type="date" name="date" class="form-control" required>
								</div>
								</td>
							</tr>
							<tr>
								<td>Party ID</td>
								<td>:</td>
								<td><input type="number" name="partyId" class="form-control" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="col-auto form-inline">
										<input style="margin-right:60px;" type="submit" class="btn btn-primary" name="search" value="Search">
										<input style="margin-right:60px;" type="button" class="btn btn-primary" name="print" value="Print">
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 