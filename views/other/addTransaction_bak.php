<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\Bsml\Bsml();
$objTransaction= new \App\Bsml\Transaction();
$allData = $objBookTitle->index();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$bankNme=$objTransaction->allbank();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;

$_SESSION['Page']= $page;
if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;
$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);
$serial = (  ($page-1) * $itemsPerPage ) +1;
if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBookTitle->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################
include_once('header.php');
?>
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">
        /* Voucher selection on Sale*/


        /* Receipt & Payment Selection*/
                function checkTransactionType(val){
            var recelement=document.getElementById('receivedFrom');
            var payelement=document.getElementById('paidTo');
            var crvoucher=document.getElementById('crVoucherNo');
            var voucher = document.getElementById('voucherNo');
            var challan = document.getElementById('challanNo');

            if(val=='MREC'){
                recelement.style.display='block';
                crvoucher.style.display='block';
                voucher.style.display='none';
                }
            else{recelement.style.display='none';}
            if(val=='MPAY'){
                payelement.style.display='block';
                voucher.style.display='block';
                crvoucher.style.display='none';
                challan.style.display='none';

            }

            else{payelement.style.display='none';}
        }

        /* Bank selection on Bank Deposit*/
        function CheckAccountHeadField(val) {
            var crvoucher=document.getElementById('crVoucherNo');
            var bank = document.getElementById('activeBankField');
            var voucher = document.getElementById('voucherNo');
            var challan = document.getElementById('challanNo');

            if (val == 36 || val == 46 || val == 47) {
                bank.style.display = 'block';
                challan.style.display = 'none';
                voucher.style.display = 'none';
                crvoucher.style.display = 'none';

            }
            else if (val == 24) {
                challan.style.display = 'block';
                voucher.style.display = 'none';
                crvoucher.style.display = 'none';
            }

            else {
                bank.style.display = 'none';
                challan.style.display = 'none';
                voucher.style.display = 'block';
                crvoucher.style.display = 'none';
            }
        }
        /* checque*/
        function ChequeField(val){
            var element=document.getElementById('activeChequeField');
            if(val=='CASH CHEQUE'){ element.style.display='block'; }
            else{element.style.display='none';  }
        }
    </script>
    <form class="form-group" name="transactionEntry" action="../store.php" method="post">
        <input hidden name="addTransaction" type="text" value="addTransaction">
        <input name="modifiedDate" type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">Transaction Type :</label> </div>
                    <div class="col-sm-4 text-left">
                        <select name="transactionType" onchange="checkTransactionType(this.value)" style="width:150px; " class="form-control">
                            <option value="MPAY">PAYMENT</option>
                            <option  value="MREC">RECEIPT</option>
                            <!--
                            <option value="JOUR">JOURNAL</option>
                            <option value="CONT">CONTRA</option>
                            -->
                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Related Client :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select name="customerId" style="width:150px; " class="form-control text-uppercase ">

                            <?php
                            foreach ($allClients as $individualClient){
                                echo  "<option class='text-uppercase' value='$individualClient->id'> $individualClient->name</option>";
                            }
                            ?>
                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Account Head :</label> </div>
                    <div class="col-sm-4 text-left ">
                     <select  name="accheadId"   onchange='CheckAccountHeadField(this.value);' class="form-control text-uppercase ">
                     <?php
                      foreach ($accountHead as $singleHead){
                      echo  "<option class='' value='$singleHead->id'>$singleHead->headnameenglish ($singleHead->id)</option>";
                      } ?>
                     </select>
                    </div>
                  <div class="col-sm-4"></div>
                </div>
                <div id="activeBankField"   style='display:none;'>
                    <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedBank">Bank Name :</label> </div>
                    <div class="col-sm-5 text-left ">
                        <select  name="bankId"  class="form-control text-uppercase ">
                            <option selected="selected" value="0">SELECT BANK</option>
                            <?php
                            foreach ($bankNme as $singlBank){
                                echo  "<option class='text-uppercase' value='$singlBank->id'> $singlBank->bankname ($singlBank->accountname) </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Branch :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="branchid" style="width:auto; " class="form-control text-uppercase ">
                            <option value="1">HEAD OFFICE</option>
                            <option value="2">YARD</option>
                        </select></div>
                    <div class="col-sm-2"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionDate">Transaction Date :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control selectDate" id="transactionDate" name="transactionDate" required placeholder="yyyy-mm-dd" onchange="" onkeypress="" type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div id="voucherNo"  class="row" style="display:block;">
                    <div class="col-sm-4 text-right form-group"><label for="voucherNo"> Voucher No (DR) :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " name="voucherNo" type="text" >
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div id="crVoucherNo"  class="row" style="display:none;">
                    <div class="col-sm-4 text-right form-group"><label for="crVoucherNo"> Voucher No (CR) :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " name="crVoucherNo" type="text" >
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div id="challanNo" class="row" style="display: none;">
                    <div class="col-sm-4 text-right form-group"><label for="challanNo"> Challan No :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control" name="challanNo" type="text" >
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionFor">  Transaction For :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="transactionFor" required  type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionMode">Transaction Mode :</label></div>
                    <div class="col-sm-2 text-left">
                        <select name="transactionMode" class="form-control" onchange='ChequeField(this.value);' required>
                            <option selected="selected" value="CASH">CASH</option>
                            <option value="CASH CHEQUE">CASH CHEQUE</option>
                            <option value="A/C PAYEE CHEQUE">A/C PAYEE CHEQUE</option>
                            <option value="ONLINE TRANSFER">ONLINE TRANSFER</option>
                            <option value="MOBILE BANK">MOBILE BANK</option>
                            <option value="PAY ORDER">PAY ORDER</option>
                            <option value="ATM">ATM</option>
                            <option value="D.D.">D.D.</option>
                            <option value="T.T.">T.T.</option>
                            <option value="OTHERS">OTHERS</option>
                        </select>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row" id="paidTo" style="display:block;" >
                    <div class="col-sm-4 text-right form-group"><label for="receivedTo">Paid To :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="receivedTo" type="text" >
                        <!-- receivedTo means Paid to -->
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                    <div id="receivedFrom" style="display:none;" class="row"  >
                        <div class="col-sm-4 text-right form-group"><label for="receivedFrom">Received From :</label></div>
                        <div class="col-sm-3 text-left">
                            <input class="form-control" name="receivedFrom"  type="text" >
                        </div>
                        <div class="col-sm-5"></div>
                    </div>

                <div id="activeChequeField" style='display:none;'>
                <div class="row" >
                    <div class="col-sm-4 text-right form-group"><label for="chequeNo">Cheque No :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control"  name="chequeNo"  type="text" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="chequeDate"> Cheque Date :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control selectDate"  name="chequeDate" id="chequeDate" placeholder="yyyy-mm-dd"  type="text" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> Remarks :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> Amount :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="amount"  id="txtAmount" required type="number">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="inWords">In Words :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control" name="inWords"></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
include('footer.php');
include('footer_script.php');


?>
